const fs = require('fs')

const address_size = 16
const tag_size = 8
const line_size = 4
const row_size = 3
const cache_size = 16
const words_per_line = 8

// const address_size = 16
// const tag_size = 8
// const line_size = 5
// const row_size = 2
// const cache_size = 32
// const words_per_line = 4


let enderecos
try {
    enderecos = require('./data/input.json')
} catch (error) {
    console.error(`input.json not found.\nError: ${error}`)
}

app = () => {
    
    const memory_accesses = prepare_memory_accesses()
    const cache = prepare_cache()

    const length = memory_accesses.length
    // const length = 20 

    for(let index = 0; index < length; index++) {
        do_cache_memory_access(cache, memory_accesses[index])
    }

    fs.writeFile("./cache.json", JSON.stringify(cache, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
        console.log("Cache file has been created");
    });

    fs.writeFile("./memory_accesses.json", JSON.stringify(memory_accesses, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
        console.log("Memory Accesses file has been created");
    });
}

prepare_memory_accesses = () => {
    const memory_accesses = []
    enderecos.forEach(end => {
        const bin = Number(end).toString('2').padStart(address_size, 0)
        
        const memory_access = {
            hex: end,
            bin: bin,
            tag: get_tag(bin),
            line: get_line(bin),
            row: get_row(bin),
        }

        memory_accesses.push(memory_access)
    });

    return memory_accesses
}

prepare_cache = () => {
    const cache_rows = {}
    for(let index = 0; index < words_per_line; index ++) {
        cache_rows[index.toString('2').padStart(row_size, 0)] = null
    }

    const cache = {}
    for(let index = 0; index < cache_size; index ++) {
        cache[index.toString('2').padStart(line_size, 0)] = {
            valid: 0,
            tag: null,
            ...cache_rows
        }
    }

    return cache
}

get_tag = (bin) => {
    return bin.substring(0, tag_size)
}

get_line = (bin) => {
    return bin.substring(bin.length - 1 - row_size - line_size, bin.length - 1 - row_size)
}

get_row = (bin) => {
    return bin.substring(bin.length - 1 - row_size, bin.length - 1)
}

do_cache_memory_access = (cache, memory_access) => {
    const cache_line = cache[memory_access.line]
    
    if(!!cache_line.valid) {
        if(cache_line.tag !== memory_access.tag) {
            memory_access.isHit = false
            get_block(cache, memory_access)
        } else {
            memory_access.isHit = true
            read_pos()
        }
    } else {
        memory_access.isHit = false
        get_block(cache, memory_access)
    }    
}

get_block = (cache, memory_access) => {
    let row = parseInt(memory_access.row, '2')
    let aux = parseInt(memory_access.hex, '16') - (row * (address_size/8)) // * 2
    console.log(memory_access)

    const cache_rows = {}
    for(let index = 0; index < words_per_line; index ++) {
        cache_rows[(index.toString('2').padStart(row_size, 0))] = Number(aux).toString('16').padStart(4, 0)
        console.log(Number(aux).toString('16').padStart(4, 0))
        aux += 2
    }

    cache[memory_access.line] = {
        valid: 1,
        tag: memory_access.tag,
        ...cache_rows
    }
}

read_pos = (memory_address) => {
    // do something 
}

app()
